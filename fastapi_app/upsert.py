from sqlalchemy.orm import Session
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy import text

from pydantic import BaseModel
from . import database


def upsert(db: Session, table: database.Base,  model: BaseModel):
    stmt = insert(table).values(dict(model))

    do_update_stmt = stmt.on_conflict_do_update(index_elements=['id'], set_=dict(model))

    db.execute(do_update_stmt)
    db.commit()

    return model
