from datetime import date
from typing import List, Optional, Dict
from pydantic import BaseModel

class VolunteerBase(BaseModel):
    first_name: str
    last_name: str
    email: str
    cell_number: str
    address_line1: str
    address_line2: str = ""
    city: str
    state: str
    zip: str
    county: str
    # lat: float  # not tracked in schemas as lat/long never get send in response
    # long: float
    max_drive_time: int = 60
    other_languages: List[str] = []
    misc_skills: List[str] = []
    trained: bool = False
    requests: str = ""

    class Config:
        orm_mode = True

class VolunteerCreate(VolunteerBase):
    id: Optional[int]
    availabilities: Optional[dict]

class Volunteer(VolunteerCreate):
    id: int
    submit_date: date = date.today()
    availabilities: Optional[dict]


class PollSite(BaseModel):
    id: int
    name: str
    address: str
    county: str
    # lat: float  # not tracked in schemas as lat/long never get send in response
    # long: float
    pct_number: str = "0"
    us_house_district: str = "0"
    state_house_district: str = "0"
    state_senate_district: str = "0"
    early_voting: bool
    election_day: bool
    voting_times: str = "04/20/2020-04/21/2020 08:00 AM-05:00 PM; 05/04/2020-05/04/2020 07:00 AM-04:00 PM"
    score: int = 0
    other_languages_needed: List[str] = []
    misc_skills_needed: List[str] = []

    class Config:
        orm_mode = True


class Availability(BaseModel):
    volunteer_id: int
    day: date
    shift: int
    is_availabile: bool
    pollsite_id: Optional[int]

    class Config:
        orm_mode = True

class AvailabilityUpdate(BaseModel):
    volunteer_id: int
    availabilities: dict

    class Config:
        orm_mode = True


class Distance(BaseModel):
    volunteer_id: int
    pollsite_id: int
    distance: int
    drive_time: Optional[int]

    class Config:
        orm_mode = True


class AssignmentSlot(BaseModel):
    pollsite_id: int
    day: date
    shift_id: int
    time: str
    volunteer_id: Optional[int]
    distance: Optional[int]

    class Config:
        orm_mode = True


class Shift(BaseModel):
    id: int
    key: str
    start_time: str
    end_time: str

    class Config:
        orm_mode = True
