from typing import List
from sqlalchemy.orm import Session
# from fastapi import BackgroundTasks
from starlette.background import BackgroundTask

from . import models, schemas, distance, availability


########
# READ #
########

# Volunteers #

# Get a volunteer by ID
def get_volunteer(db: Session, v_id: int):
    return db.query(models.Volunteer).filter(models.Volunteer.id == v_id).first()

# Get all volunteers
def get_volunteers(db: Session, skip: int = 0, limit: int = -1):
    if (limit > 0):
        return db.query(models.Volunteer).order_by(models.Volunteer.last_name.asc()).offset(skip).limit(limit).all()
    else:
        return db.query(models.Volunteer).order_by(models.Volunteer.last_name.asc()).offset(skip).all()


##########
# CREATE #
##########

# Create and insert a new volunteer into the database
def create_volunteer(db: Session, volunteer: schemas.VolunteerCreate):
    # get lat and long from distance.py
    lat, long = distance.geocode(volunteer)

    # Parse language and skills
    if len(volunteer.other_languages) == 1:
        volunteer.other_languages = volunteer.other_languages[0].split(', ')
    if len(volunteer.misc_skills) == 1:
        volunteer.misc_skills = volunteer.misc_skills[0].split(', ')

    # Set aside availabilities
    volunteer_availabilities = volunteer.availabilities

    volunteer = models.Volunteer(
        first_name=volunteer.first_name,
        last_name=volunteer.last_name,
        email=volunteer.email,
        cell_number=volunteer.cell_number,
        address_line1=volunteer.address_line1,
        address_line2=volunteer.address_line2,
        city=volunteer.city,
        state=volunteer.state,
        county=volunteer.county,
        zip=volunteer.zip,
        lat=lat,
        long=long,
        max_drive_time=volunteer.max_drive_time,
        other_languages=volunteer.other_languages,
        misc_skills=volunteer.misc_skills,
        trained=volunteer.trained,
        requests=volunteer.requests,
        availabilities=volunteer.availabilities
    )

    db.add(volunteer)
    db.commit()
    db.refresh(volunteer)

    # Invoke distance functions
    distance.create_volunteer(db, volunteer)

    # Invoke availability functions
    availability.handle_availabilities(db, volunteer_availabilities, volunteer.id)

    return volunteer


##########
# UPDATE #
##########

# Update a volunteer
def update_volunteer(db: Session, volunteer: schemas.Volunteer):
    db_volunteer = db.query(models.Volunteer).filter(models.Volunteer.id == volunteer.id).first()

    # Save availabilities, there is separate function for updating these
    volunteer.availabilities = db_volunteer.availabilities

    # Check for address change before updating
    address_change = (db_volunteer.address_line1 != volunteer.address_line1)

    # Perform the update
    db.query(models.Volunteer).filter(models.Volunteer.id == volunteer.id).update(volunteer)
    db.commit()
    db.refresh(db_volunteer)

    # Update geocoding and distance entries
    if (address_change):
        db_volunteer.lat, db_volunteer.long = distance.geocode(volunteer)
        db.commit()
        db.refresh(db_volunteer)

        distance.recalculate_volunteer(db, db_volunteer)

    return volunteer

# Update volunteer availabilities only
def update_volunteer_availability(db: Session, availabilities: schemas.AvailabilityUpdate):
    volunteer = db.query(models.Volunteer).filter(models.Volunteer.id == availabilities.volunteer_id).first()
    volunteer.availabilities = availabilities.availabilities
    db.commit()
    db.refresh(volunteer)

    availability.handle_availabilities(db, availabilities.availabilities, availabilities.volunteer_id)

    return volunteer

##########
# DELETE #
##########

# Delete a volunteer
def delete_volunteer(db: Session, v_id: int):
    volunteer = db.query(models.Volunteer).filter(models.Volunteer.id == v_id).first()
    db.delete(volunteer)
    db.commit()

    return volunteer


#########
# OTHER #
#########

# Find potential duplicate entries
def find_duplicates(db: Session):
    return
