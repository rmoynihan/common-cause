# per day

    # for each poll site
        # for each shift
            # insert row into assignment table

    # Iterate over slots of this day
        # iterate over volunteers (ordered by distance, filtered by availability (marked available / not assigned already)  and max_drive_time)
            # iterate over sublist (filtered again by language / skills)

                # get first volunteer of this list
                # set the availability object with (v_id, day, shift) p_id = pollsite.id
                # set the assignment object with (day, p_id, shift) v_id = volunteer.id

            # if none found, remove language / skill filter, iterate again
                # get first volunteer of this list
                # set the availability object with (v_id, day, shift) p_id = pollsite.id
                # set the assignment object with (day, p_id, shift) v_id = volunteer.id


    # assignments
        # PK: day, p_id, shift  # FK: v_id


    # select * from assignment where day = day
    # send via json

from datetime import datetime, timedelta
import time
from sqlalchemy.orm import Session
from sqlalchemy import func

from . import models, schemas, email


# Return the pre-generated assignments for a given day (ordered by pollsite priority)
def get_assignments(db: Session, day: str):
    slots = (db.query(models.AssignmentSlot).join(models.PollSite)
                .filter(
                    models.AssignmentSlot.day == day,
                    models.AssignmentSlot.volunteer_id != None
                ).order_by(models.PollSite.score.desc()).all())
    return slots

# Given a day, generate volunteer assignments for each pollsite, for each shift
def generate_assignments(db: Session, day: str, email_address: str):
    # Set volunteer availabilities back to open
    reset_volunteer_availability(db, day)

    # Reset assignment slots back to unfilled
    reset_assignment_slots(db, day)

    # Create the assignment slots that we need to fill
    slots = (db.query(models.AssignmentSlot).join(models.PollSite)
                .filter(
                    models.AssignmentSlot.day == day
                ).order_by(models.PollSite.score.desc()).all())

    prev_volunteer_id = None
    prev_pollsite_id = None
    prev_pollsite = None
    for slot in slots:
        if slot.pollsite_id != prev_pollsite_id:
            pollsite = db.query(models.PollSite).filter(models.PollSite.id == slot.pollsite_id).first()
        else:
            pollsite = prev_pollsite

        # If this is the next shift for the same pollsite, check if the volunteer assigned to the previous shift is availabile again (continuous assignment)
        if (pollsite == prev_pollsite):
            prev_vol_availabilities = db.query(models.Availability).filter(
                models.Availability.volunteer_id == prev_volunteer_id,
                models.Availability.day == day,
                models.Availability.shift_id == slot.shift_id
            ).all()
            if len(prev_vol_availabilities) > 0:
                prev_volunteer_id = assign_volunteer(db, slot, prev_vol_availabilities[0])
                continue

        # Find all volunteer availabilities that meet criteria
        availabilities = (db.query(models.Availability)
                        .join(models.Distance, models.Distance.volunteer_id == models.Availability.volunteer_id)
                        .join(models.Volunteer, models.Volunteer.id == models.Availability.volunteer_id)
                        .filter(
                            models.Availability.day == day,
                            models.Availability.shift_id == slot.shift_id,
                            models.Availability.pollsite_id == None,
                            models.Distance.pollsite_id == slot.pollsite_id,
                            # models.Volunteer.trained == True,
                            models.Distance.distance <= models.Volunteer.max_drive_time
                        )
                        .order_by(models.Distance.distance.asc())
                        .all())

        # Check if there were any availabilities found
        if len(availabilities) > 0:
            # Test if pollsite has any special requirements; if not, just assign the top volunteer
            if len(pollsite.other_languages_needed) == 0 and len(pollsite.misc_skills_needed) == 0:
                prev_volunteer_id = assign_volunteer(db, slot, availabilities[0])
                continue

            assigned = False
            # First, check for both language and skills
            if not assigned:
                for availability in availabilities:
                    volunteer = db.query(models.Volunteer).filter(models.Volunteer.id == availability.volunteer_id).first()

                    # Ideally, volunteer possess necessary language and skills
                    if language_check(db, volunteer, pollsite) and skills_check(db, volunteer, pollsite):
                        prev_volunteer_id = assign_volunteer(db, slot, availability)
                        assigned = True
                        break

            # Next, language only
            if not assigned:
                for availability in availabilities:
                    volunteer = db.query(models.Volunteer).filter(models.Volunteer.id == availability.volunteer_id).first()

                    # Ideally, volunteer possess necessary language and skills
                    if language_check(db, volunteer, pollsite):
                        prev_volunteer_id = assign_volunteer(db, slot, availability)
                        assigned = True
                        break

            # Then, skills only
            if not assigned:
                for availability in availabilities:
                    volunteer = db.query(models.Volunteer).filter(models.Volunteer.id == availability.volunteer_id).first()

                    # Ideally, volunteer possess necessary language and skills
                    if skills_check(db, volunteer, pollsite):
                        prev_volunteer_id = assign_volunteer(db, slot, availability)
                        assigned = True
                        break

            # Finally, just assign first available
            if not assigned:
                prev_volunteer_id = assign_volunteer(db, slot, availabilities[0])

        # Mark pollsite as previous
        prev_pollsite_id = pollsite.id
        prev_pollsite = pollsite

    email.notify_assignments_ready(day, email_address)
    return get_assignments(db, day)


# Assign a volunteer to an AssignmentSlot, and set the pollsite field in the volunteer's Availability row to indicate assignment
def assign_volunteer(db: Session, slot: models.AssignmentSlot, availability: models.Availability):
    slot.volunteer_id = availability.volunteer_id
    availability.pollsite_id = slot.pollsite_id
    slot.distance = (db.query(models.Distance)
        .filter(models.Distance.volunteer_id == availability.volunteer_id, models.Distance.pollsite_id == availability.pollsite_id).first()).distance

    db.commit()


# Check if a given volunteer meets the language and skill requirements for a given pollsite
def language_check(db: Session, volunteer: models.Volunteer, pollsite: models.PollSite):
    # Perform the checks
    if (set(volunteer.other_languages) & set(pollsite.other_languages_needed)):
        return True

    return False


# Check if a given volunteer meets the language and skill requirements for a given pollsite
def skills_check(db: Session, volunteer: models.Volunteer, pollsite: models.PollSite):
    # Perform the checks
    if (set(volunteer.misc_skills) & set(pollsite.misc_skills_needed)):
        return True

    return False


# Remove any existing pollsite id from volunteer availability rows (needed if re-running an assignment)
def reset_volunteer_availability(db: Session, day: str):
    availabilities = db.query(models.Availability).filter(models.Availability.day == day, models.Availability.pollsite_id is not None).update({models.Availability.pollsite_id: None})
    db.commit()
    # for availability in availabilities:
    #     availability.pollsite_id = None
    #     db.commit()


# Reset assignment slots back to unfilled
def reset_assignment_slots(db: Session, day: str):
    # slots_count = db.query(models.AssignmentSlot).filter(models.AssignmentSlot.day == day).count()
    # if (slots_count == 0):
    create_assignment_slots(db, day)
    return

    slots = db.query(models.AssignmentSlot).filter(models.AssignmentSlot.day == day).update({models.AssignmentSlot.volunteer_id: None})
    db.commit()

# Generate unassigned slots to fill in volunteers with
def create_assignment_slots(db: Session, day: str):
    print("Creating slots for day: " + day)
    # Delete slots of already exist for this day
    slots = db.query(models.AssignmentSlot).filter(models.AssignmentSlot.day == day).delete()

    # Generate assignment slots for all pollsites for all shifts
    pollsites = db.query(models.PollSite).order_by(models.PollSite.score.desc()).all()
    shifts = db.query(models.Shift).all()

    # Create the slots
    slots = []
    for pollsite in pollsites:
        for shift in shifts:
            # Check if the pollsite needs this shift, and alter time string if the shift is partial
            shift_needed, time_string = needs_shift(pollsite, shift, day)
            if shift_needed:
                slot = models.AssignmentSlot(
                    pollsite_id=pollsite.id,
                    day=day,
                    shift_id=shift.id,
                    time=time_string
                )
                db.add(slot)
                db.commit()
                db.refresh(slot)
                slots.append(slot)

    return slots

# Check a pollsite's voting datetime string to see if a shift needs to be created
# Also determines if an alternate time string is required (partial shifts)
def needs_shift(pollsite: models.PollSite, shift: models.Shift, day: str):
    # Parse the nasty time string
    voting_times_dict = parse_voting_times(pollsite.voting_times)

    # Convert day to datetime.date object (helps ignore leading zeros and such)
    day = datetime.strptime(day, '%m/%d/%Y').date()

    # Make checks
    if day in voting_times_dict:
        pollsite_open_time = voting_times_dict[day][0]
        pollsite_close_time = voting_times_dict[day][1]

        # Parse the start and end time of the shift into Python Time types
        shift_start, shift_end = parse_time_range(shift.start_time + "-" + shift.end_time)

        # Initial check: is there any overlap between the shift and when the pollsite is open?
        if (shift_end > pollsite_open_time and shift_start < pollsite_close_time):
            # There is overlap, but we now need to check if any shifts extend either before or after when the pollsite is open
            # First, set the time string as if the shift is fully contained in the time window the pollsite is open
            time_string = time.strftime("%I:%M %p-", shift_start) + time.strftime("%I:%M %p", shift_end)

            # Check if shift starts before pollsite is open
            if (shift_start < pollsite_open_time):
                # Set time string so the start is now the pollsite open time, not shift start time
                time_string = time.strftime("%I:%M %p-", pollsite_open_time) + time.strftime("%I:%M %p", shift_end)

            # Check if shift ends after pollsite closes
            elif (shift_end > pollsite_close_time):
                time_string = time.strftime("%I:%M %p-", shift_start) + time.strftime("%I:%M %p", pollsite_close_time)

            # Return
            return True, time_string

    # Pollsite does not need anyone during this shift (no overlap)
    return False, None

# Parse this gnarly beast
# 02/18/2020-02/21/2020 08:00 AM-05:00 PM; 02/24/2020-02/28/2020 08:00 AM-05:00 PM
# Ideally this is checked by REGEX in pollsite create/update, and on frontend
def parse_voting_times(voting_times: str):
    # Split up the date ranges
    ranges = voting_times.split(';')

    dt_dict = {}
    for r in ranges:
        r = r.strip()

        # Separate date range and time range for those dates
        days = r.split(' ', 1)[0]
        time = r.split(' ', 1)[1]

        # Split start and end time
        stime, etime = parse_time_range(time)

        # Split start and end date
        sdate, edate = days.split('-')

        # Convert them to actual date objects
        sdate = datetime.strptime(sdate, '%m/%d/%Y').date()
        edate = datetime.strptime(edate, '%m/%d/%Y').date()
        delta = edate - sdate

        # Generate all days between them and add to dictionary
        for i in range(delta.days + 1):
            day = sdate + timedelta(days=i)
            # day = datetime.strftime(day, '%m/%d/%Y')
            dt_dict[day] = (stime, etime)

    return dt_dict

def parse_time_range(time_r: str):
    # Split start and end time
    stime, etime = time_r.split('-')

    # Convert to actual time objects
    stime = time.strptime(stime, '%I:%M %p')
    etime = time.strptime(etime, '%I:%M %p')

    return stime, etime

