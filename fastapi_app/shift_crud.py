from sqlalchemy.orm import Session

from . import models, schemas, assignment_slots


########
# READ #
########

# Get a shift from the database
def get_shift(db: Session, shift_id: int = None, shift_key: str = None):
    if shift_id:
        db_shift = db.query(models.Shift).filter(models.Shift.id == shift_id).first()
    elif shift_key:
        db_shift = db.query(models.Shift).filter(models.Shift.key == shift_key).first()

    return db_shift


# Get all shifts
def get_shifts(db: Session):
    shifts = db.query(models.Shift).all()

    return shifts


##########
# CREATE #
##########

# Create a new shift in the database
def create_shift(db: Session, shift: schemas.Shift):
    shift = models.Shift(
        key=shift.key,
        start_time=shift.start_time,
        end_time=shift.end_time
    )

    db.add(shift)
    db.commit()
    db.refresh(shift)

    # assignment_slots.update_slots_shift(db, shift.id)

    return shift


##########
# UPDATE #
##########

# Update a shift in the database
def update_shift(db: Session, shift: schemas.Shift):
    db_shift = db.query(models.Shift).filter(models.Shift.id == shift.id).first()

    # Perform the update
    db.query(models.Shift).filter(models.Shift.id == shift.id).update(shift)
    db.commit()
    db.refresh(db_shift)

    # assignment_slots.update_slots_shift(db, db_shift.id)

    return db_shift


##########
# DELETE #
##########

# Delete a shift in the database
def delete_shift(db: Session, shift_id: int):
    db_shift = db.query(models.Shift).filter(models.Shift.id == shift_id).first()
    db.delete(db_shift)
    db.commit()

    # assignment_slots.delete_shift(db, db_shift.id)

    return db_shift
