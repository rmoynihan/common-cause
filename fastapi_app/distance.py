from sqlalchemy.orm import Session
from random import randint
from math import radians, cos, sin, asin, sqrt
import geocoder

from . import schemas, models
from .api import gmaps, MAPQUEST_API_KEY


# Use ___ API to geocode address
def geocode(obj: schemas.BaseModel):
    # Build address string
    try:
        address = obj.address_line1 + obj.address_line2 + ',' + obj.city + ',' + obj.state + ',' + obj.zip
    except AttributeError:
        address = obj.address

    try:
        g = geocoder.mapquest(address, key=MAPQUEST_API_KEY)
        lat = g.lat
        long = g.lng
    except IndexError:
        try:
            geocode_result = gmaps.geocode(address)
            lat = geocode_result[0]['geometry']['location']['lat']
            long = geocode_result[0]['geometry']['location']['lng']
        except:
            print('geocode error')
            return 0, 0


    # Return lat, long
    return lat, long
    # return randint(0, 360), randint(0, 360)


# Use the Haversine formula to calculate distance 'as the crow flies'
def calc_distance(v_lat: float, v_long: float, p_lat: float, p_long: float):
    # Convert decimal degrees to radians
    v_lat, v_long, p_lat, p_long = map(radians, [v_lat, v_long, p_lat, p_long])

    # Haversine formula
    dlong = p_long - v_long
    dlat = p_lat - v_lat
    a = sin(dlat/2)**2 + cos(v_lat) * cos(p_lat) * sin(dlong/2)**2
    c = 2 * asin(sqrt(a))
    # r = 6371 # Radius of the earth in kilometers
    r = 3956 # Radius of the earth in miles
    return c * r


# Use ___ API to get drive time between two addresses
def get_drive_time(volunteer: schemas.Volunteer, pollsite: schemas.PollSite):
    # build address string
    # call API
    # return results
    return randint(0, 60)


# Generate and insert models for distance matrix
def create_volunteer(db: Session, volunteer: schemas.Volunteer):
    # Get all pollsites in DB
    pollsites = db.query(models.PollSite).all()

    # Calculate and store distances
    for pollsite in pollsites:
        distance = calc_distance(volunteer.lat, volunteer.long, pollsite.lat, pollsite.long)
        drive_time = get_drive_time(volunteer, pollsite)

        distance_obj = models.Distance(
                                      volunteer_id=volunteer.id,
                                      pollsite_id=pollsite.id,
                                      distance=distance,
                                      drive_time=drive_time
                                      )

        db.add(distance_obj)

    db.commit()


# Generate and insert models for distance matrix
def create_pollsite(db: Session, pollsite: schemas.PollSite):
    # Get all pollsites in DB
    volunteers = db.query(models.Volunteer).all()

    # Calculate and store distances
    for volunteer in volunteers:
        distance = calc_distance(volunteer.lat, volunteer.long, pollsite.lat, pollsite.long)
        drive_time = get_drive_time(volunteer, pollsite)

        distance_obj = models.Distance(
                                      volunteer_id=volunteer.id,
                                      pollsite_id=pollsite.id,
                                      distance=distance,
                                      drive_time=drive_time
                                      )

        db.add(distance_obj)

    db.commit()


# Recalculate distances when a volunteer address is updated
def recalculate_volunteer(db: Session, volunteer: models.Volunteer):
    # Get all pollsites in DB
    pollsites = db.query(models.PollSite).all()

    # Calculate and update distances
    for pollsite in pollsites:
        distance = calc_distance(volunteer.lat, volunteer.long, pollsite.lat, pollsite.long)
        drive_time = get_drive_time(volunteer, pollsite)

        distance_obj = db.query(models.Distance).filter(models.Distance.volunteer_id == volunteer.id, models.Distance.pollsite_id == pollsite.id).first()

        distance_obj.distance = distance
        distance_obj.drive_time = drive_time

        db.commit()


# Recalculate distances when a pollsite address is updated
def recalculate_pollsite(db: Session, pollsite: schemas.PollSite):
    # Get all volunteers in DB
    volunteers = db.query(models.Volunteer).all()

    # Calculate and update distances
    for volunteer in volunteers:
        distance = calc_distance(volunteer.lat, volunteer.long, pollsite.lat, pollsite.long)
        drive_time = get_drive_time(volunteer, pollsite)

        distance_obj = db.query(models.Distance).filter(models.Distance.volunteer_id == volunteer.id, models.Distance.pollsite_id == pollsite.id).first()

        distance_obj.distance = distance
        distance_obj.drive_time = drive_time

    db.commit()
