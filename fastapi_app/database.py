from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool

from . import models


Base = declarative_base()

IP_ADDRESS = "10.126.80.3"
PORT = "5432"
USERNAME = "postgres"
PASSWORD = "commoncause"
DATABASE_URL_BASE = "postgresql://" + USERNAME + ":" + PASSWORD + "@" + IP_ADDRESS + ":" + PORT + "/"

# Builds the database connection with the given bind (state)
def build_sessionlocal(dbname: str):

    SQLALCHEMY_DATABASE_URL = DATABASE_URL_BASE + dbname

    engine = engines[SQLALCHEMY_DATABASE_URL]
    SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    # print("DB: {db}".format(db=dbname))

    return engine, SessionLocal

# Initializes the tables based on models.py
engines = {}
def init_tables():
    for bind in binds:
        URI = DATABASE_URL_BASE + bind
        engine = create_engine(URI, poolclass=NullPool) # NullPool extremely important, otherwise each engine will create it's own pool
                                                        # and the DB will run out of connection slots
        engines[URI] = engine
        models.Base.metadata.create_all(bind=engine)


# All 'binds', or databases (should be one for every state, plus default)
binds = [
    'dev',
    # 'texas',
    # 'california',
    'unit-testing',
]
