from sqlalchemy.orm import Session
from fastapi import HTTPException
from typing import List

from . import models


# Query and format a volunteer's availabilities for response
# def get_availabilities(db: Session, v_id):
#     response_dict = {}
#     for availability, shift in db.query(models.Availability, models.Shift).filter(models.Availability.volunteer_id == v_id).all():
#         if availability.day in response_dict:
#             response_dict[availability.day] += ", " + shift.time
#         else:
#             response_dict[availability.day] = shift.time
#
#     return response_dict


# Perform all functions necessary to manage a volunteer's availabilities in the database
# No updating, simply delete existing ones and add them back correctly
def handle_availabilities(db: Session, availabilities: dict, v_id: int):
    availabilities = parse_availabilities_string(db, availabilities)
    for day, shifts in availabilities.items():
        delete_existing(db, day, v_id)
        create_and_add_availabilities(db, day, shifts, v_id)

# Create the availability objects and add them to the DB
def create_and_add_availabilities(db: Session, day: str, shifts: List[int], v_id: int):
    for shift in shifts:
        availability = models.Availability(
                                          volunteer_id=v_id,
                                          day=day,
                                          shift_id=shift
                                          )
        db.add(availability)
    db.commit()

# Delete existing availability objects for the given day
def delete_existing(db: Session, day: str, v_id: int):
    existing_availabilities = db.query(models.Availability).filter(models.Availability.day == day, models.Availability.volunteer_id == v_id).all()
    for availability in existing_availabilities:
        db.delete(availability)
    db.commit()

# Parse the key-value pair and convert the value (string of shifts) to a list of shift IDs
def parse_availabilities_string(db: Session, availabilities: dict):
    for day in list(availabilities):
        # Check for any empty values
        if availabilities[day] == "":
            del availabilities[day]
            continue

        shifts = availabilities[day].split(", ")
        try:
            availabilities[day] = [db.query(models.Shift).filter(models.Shift.key == shift).first().id for shift in shifts]
        # Shift doesn't exist
        except AttributeError:
            raise HTTPException(status_code=400, detail="A shift key in [{key}] was not recognized".format(key=availabilities[day]))


    return availabilities
