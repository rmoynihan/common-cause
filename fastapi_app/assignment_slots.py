from typing import List
from datetime import datetime, timedelta
import time
from sqlalchemy.orm import Session

from . import models, schemas, assignment

def update_slots_all(db: Session):
    days = get_all_days(db)
    for day in days:
        day = day[0].strftime("%m/%d/%Y")
        slots = assignment.create_assignment_slots(db, day)
    db.commit()
    return True


# Get unique days
def get_all_days(db: Session):
    return db.query(models.Availability.day).distinct()


# # Remake all assignment slots
# def update_slots_shift(db: Session, shift_id: int = None):
#     # slots = db.query(models.AssignmentSlot).filter(models.AssignmentSlot.shift_id == shift_id).all()
#     print('starting update slots shift')
#     delete_slots_shift(db, shift_id)
#     shift = db.query(models.Shift).filter(models.Shift.id == shift_id).first()
#     days = get_all_days(db)
#
#     for day in days:
#         day = day[0].strftime("%m/%d/%Y")
#         create_assignment_slots(db, day, pollsites=None, shifts=[shift])
#     db.commit()
#     print('slots update done')
#
#
# # Remake assignment slots for a single pollsite
# def update_slots_pollsite(db: Session, pollsite_id: int):
#     delete_slots_pollsite(db, pollsite_id)
#     pollsite = db.query(models.PollSite).filter(models.PollSite.id == pollsite_id).first()
#     days = get_all_days(db)
#
#     for day in days:
#         day = day[0].strftime("%m/%d/%Y")
#         create_assignment_slots(db, day, pollsites=[pollsite], shifts=None)
#     db.commit()
#
# def delete_slots_shift(db: Session, shift_id: int):
#     db.query(models.AssignmentSlot).filter(models.AssignmentSlot.shift_id == shift_id).delete()
#     db.commit()
#
# def delete_slots_pollsite(db: Session, pollsite_id: int):
#     db.query(models.AssignmentSlot).filter(models.AssignmentSlot.pollsite_id == pollsite_id).delete()
#     db.commit()
#
#
# # Generate unassigned slots to fill in volunteers with
# def create_assignment_slots(db: Session, day: str, pollsites: List[models.PollSite] = None, shifts: List[models.Shift] = None):
#     # Generate assignment slots for all pollsites for all shifts
#     if pollsites is None:
#         pollsites = db.query(models.PollSite).all()
#     if shifts is None:
#         shifts = db.query(models.Shift).all()
#     # Create the slots
#     slots = []
#     for pollsite in pollsites:
#         old_slots = db.query(models.AssignmentSlot).filter(models.AssignmentSlot.day == day, models.AssignmentSlot.pollsite_id == pollsite.id).delete()
#         for shift in shifts:
#             # Check if the pollsite needs this shift, and alter time string if the shift is partial
#             shift_needed, time_string = needs_shift(pollsite, shift, day)
#             if shift_needed:
#                 slot = models.AssignmentSlot(
#                     pollsite_id=pollsite.id,
#                     day=day,
#                     shift_id=shift.id,
#                     time=time_string
#                 )
#                 db.add(slot)
#                 db.commit()
#                 db.refresh(slot)
#                 slots.append(slot)
#
#     return slots
#
# # Check a pollsite's voting datetime string to see if a shift needs to be created
# # Also determines if an alternate time string is required (partial shifts)
# def needs_shift(pollsite: models.PollSite, shift: models.Shift, day: str):
#     # Parse the nasty time string
#     voting_times_dict = parse_voting_times(pollsite.voting_times)
#
#     # Convert day to datetime.date object (helps ignore leading zeros and such)
#     day = datetime.strptime(day, '%m/%d/%Y').date()
#
#     # Make checks
#     if day in voting_times_dict:
#         pollsite_open_time = voting_times_dict[day][0]
#         pollsite_close_time = voting_times_dict[day][1]
#
#         # Parse the start and end time of the shift into Python Time types
#         shift_start, shift_end = parse_time_range(shift.start_time + "-" + shift.end_time)
#
#         # Initial check: is there any overlap between the shift and when the pollsite is open?
#         if (shift_end > pollsite_open_time and shift_start < pollsite_close_time):
#             # There is overlap, but we now need to check if any shifts extend either before or after when the pollsite is open
#             # First, set the time string as if the shift is fully contained in the time window the pollsite is open
#             time_string = time.strftime("%I:%M %p-", shift_start) + time.strftime("%I:%M %p", shift_end)
#
#             # Check if shift starts before pollsite is open
#             if (shift_start < pollsite_open_time):
#                 # Set time string so the start is now the pollsite open time, not shift start time
#                 time_string = time.strftime("%I:%M %p-", pollsite_open_time) + time.strftime("%I:%M %p", shift_end)
#
#             # Check if shift ends after pollsite closes
#             elif (shift_end > pollsite_close_time):
#                 time_string = time.strftime("%I:%M %p-", shift_start) + time.strftime("%I:%M %p", pollsite_close_time)
#
#             # Return
#             return True, time_string
#
#     # Pollsite does not need anyone during this shift (no overlap)
#     return False, None
#
# # Parse this gnarly beast
# # 02/18/2020-02/21/2020 08:00 AM-05:00 PM; 02/24/2020-02/28/2020 08:00 AM-05:00 PM
# # Ideally this is checked by REGEX in pollsite create/update, and on frontend
# def parse_voting_times(voting_times: str):
#     # Split up the date ranges
#     ranges = voting_times.split(';')
#
#     dt_dict = {}
#     for r in ranges:
#         r = r.strip()
#
#         # Separate date range and time range for those dates
#         days = r.split(' ', 1)[0]
#         time = r.split(' ', 1)[1]
#
#         # Split start and end time
#         stime, etime = parse_time_range(time)
#
#         # Split start and end date
#         sdate, edate = days.split('-')
#
#         # Convert them to actual date objects
#         sdate = datetime.strptime(sdate, '%m/%d/%Y').date()
#         edate = datetime.strptime(edate, '%m/%d/%Y').date()
#         delta = edate - sdate
#
#         # Generate all days between them and add to dictionary
#         for i in range(delta.days + 1):
#             day = sdate + timedelta(days=i)
#             # day = datetime.strftime(day, '%m/%d/%Y')
#             dt_dict[day] = (stime, etime)
#
#     return dt_dict
#
# def parse_time_range(time_r: str):
#     # Split start and end time
#     stime, etime = time_r.split('-')
#
#     # Convert to actual time objects
#     stime = time.strptime(stime, '%I:%M %p')
#     etime = time.strptime(etime, '%I:%M %p')
#
#     return stime, etime
