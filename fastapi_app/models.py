import datetime
from sqlalchemy import Boolean, Column, ForeignKey, Integer, Float, String, Date, DateTime
from sqlalchemy.dialects.postgresql import ARRAY, JSON
from sqlalchemy.orm import relationship, backref

from .database import Base


class Volunteer(Base):
    __tablename__ = "volunteer"

    id = Column(Integer, primary_key=True, index=True)
    submit_date = Column(Date, default=datetime.date.today())
    first_name = Column(String)
    last_name = Column(String)
    email = Column(String)
    cell_number = Column(String)
    address_line1 = Column(String)
    address_line2 = Column(String, default="")
    city = Column(String)
    state = Column(String)
    zip = Column(String)
    county = Column(String)
    lat = Column(Float)
    long = Column(Float)
    max_drive_time = Column(Integer, default=60)
    other_languages = Column(ARRAY(String), default=None)
    misc_skills = Column(ARRAY(String), default=None)
    trained = Column(Boolean, default=False)
    requests = Column(String)
    availabilities = Column(JSON, default=None)


class PollSite(Base):
    __tablename__ = "pollsite"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    address = Column(String)
    county = Column(String)
    lat = Column(Float)
    long = Column(Float)
    pct_number = Column(String)
    us_house_district = Column(String)
    state_house_district = Column(String)
    state_senate_district = Column(String)
    early_voting = Column(Boolean)
    election_day = Column(Boolean)
    voting_times = Column(String)
    score = Column(Integer)
    other_languages_needed = Column(ARRAY(String), default=None)
    misc_skills_needed = Column(ARRAY(String), default=None)


class Shift(Base):
    __tablename__ = "shift"

    id = Column(Integer, primary_key=True, index=True)
    key = Column(String, unique=True, index=True)
    start_time = Column(String)
    end_time = Column(String)


class Availability(Base):
    __tablename__ = "availability"

    volunteer_id = Column(Integer, ForeignKey('volunteer.id', ondelete='CASCADE'), primary_key=True)
    day = Column(Date, primary_key=True)
    shift_id = Column(Integer, ForeignKey('shift.id'), primary_key=True)

    pollsite_id = Column(Integer, ForeignKey('pollsite.id', ondelete='CASCADE'), nullable=True)

    volunteer = relationship(Volunteer, backref=backref("volunteer_availabilities", cascade="all,delete", uselist=False, passive_deletes=True))
    pollsite = relationship(PollSite, backref=backref("pollsite_availabilities", cascade="all,delete", uselist=False))
    shift = relationship(Shift, backref=backref("avail_shifts", cascade="all,delete"))


class Distance(Base):
    __tablename__ = "distance"

    volunteer_id = Column(Integer, ForeignKey('volunteer.id', ondelete='CASCADE'), primary_key=True)
    pollsite_id = Column(Integer, ForeignKey('pollsite.id', ondelete='CASCADE'), primary_key=True)

    distance = Column(Integer, default=0)
    drive_time = Column(Integer, default=0)

    volunteer = relationship(Volunteer, backref=backref("distances", cascade="all,delete"))
    pollsite = relationship(PollSite, backref=backref("distances", cascade="all,delete"))


class AssignmentSlot(Base):
    __tablename__ = "assignment"

    pollsite_id = Column(Integer, ForeignKey('pollsite.id', ondelete='CASCADE'), primary_key=True)
    day = Column(Date, primary_key=True)
    shift_id = Column(Integer, ForeignKey('shift.id', ondelete='CASCADE'), primary_key=True)

    time = Column(String, nullable=True, default=None)
    volunteer_id = Column(Integer, ForeignKey('volunteer.id'), nullable=True)
    distance = Column(Integer, nullable=True)

    volunteer = relationship(Volunteer, backref=backref("assignments", cascade="all,delete"))
    pollsite = relationship(PollSite, backref=backref("assignments", cascade="all,delete"))
    shift = relationship(Shift, backref=backref("assign_shifts", cascade="all,delete"))
