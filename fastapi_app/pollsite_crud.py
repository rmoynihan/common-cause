from sqlalchemy.orm import Session
from fastapi import BackgroundTasks

from . import models, schemas, distance, availability, assignment_slots


########
# READ #
########

# Get a poll site by ID
def get_pollsite(db: Session, p_id: int):
    return db.query(models.PollSite).filter(models.PollSite.id == p_id).first()

# Get all poll sites
def get_pollsites(db: Session, skip: int = 0, limit: int = -1):
    if (limit > 0):
        return db.query(models.PollSite).order_by(models.PollSite.score.desc()).offset(skip).limit(limit).all()
    else:
        return db.query(models.PollSite).order_by(models.PollSite.score.desc()).offset(skip).all()


##########
# CREATE #
##########

# Create and insert a new poll site into the database
def create_pollsite(db: Session, pollsite: schemas.PollSite):
    # get lat and long from distance.py
    lat, long = distance.geocode(pollsite)

    # Determine the priority of this pollsite for assignments
    pollsite = models.PollSite(
      name=pollsite.name,
      address=pollsite.address,
      county=pollsite.county,
      lat=lat,
      long=long,
      pct_number=pollsite.pct_number,
      us_house_district=pollsite.us_house_district,
      state_house_district=pollsite.state_house_district,
      state_senate_district=pollsite.state_senate_district,
      early_voting=pollsite.early_voting,
      election_day=pollsite.election_day,
      voting_times=pollsite.voting_times,
      score=pollsite.score,
      other_languages_needed=pollsite.other_languages_needed,
      misc_skills_needed=pollsite.misc_skills_needed
    )

    db.add(pollsite)
    db.commit()
    db.refresh(pollsite)

    # Invoke distance function
    distance.create_pollsite(db, pollsite)
    # assignment_slots.update_slots_pollsite(db, pollsite.id)

    return pollsite


##########
# UPDATE #
##########

# Update a poll site
def update_pollsite(db: Session, pollsite: schemas.PollSite):
    db_pollsite = db.query(models.PollSite).filter(models.PollSite.id == pollsite.id).first()

    # Check for address change before updating
    address_change = (db_pollsite.address != pollsite.address)
    voting_times_change = (db_pollsite.voting_times != pollsite.voting_times)

    # Perform the update
    db.query(models.PollSite).filter(models.PollSite.id == pollsite.id).update(pollsite)
    db.commit()
    db.refresh(db_pollsite)

    # Update geocoding and distance entries
    if (address_change):
        db_pollsite.lat, db_pollsite.long = distance.geocode(pollsite)
        db.commit()
        db.refresh(db_pollsite)

        # Invoke distance function
        distance.recalculate_pollsite(db, db_pollsite)

    if (voting_times_change):
        pass
        # assignment_slots.update_slots_pollsite(db, db_pollsite.id)

    return pollsite


##########
# DELETE #
##########

# Delete a poll site
def delete_pollsite(db: Session, p_id: int):
    pollsite = db.query(models.PollSite).filter(models.PollSite.id == p_id).first()
    db.delete(pollsite)
    db.commit()

    # assignment_slots.delete_slots_pollsite(db, pollsite.id)

    return pollsite
