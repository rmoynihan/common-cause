from typing import List
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from . import models, schemas

MAIL_USERNAME = "cc.vm.system@gmail.com"
MAIL_PASSWORD = "commoncause1970"

def notify_assignments_ready(day: str, email: str):
    mail = smtplib.SMTP('smtp.gmail.com', 587)
    mail.ehlo()
    mail.starttls()
    mail.login(MAIL_USERNAME, MAIL_PASSWORD)

    message_txt = """Your assignments for """ + day + """ have been generated."""
    message_txt += """\n\nIf they are not automatically retrieved, use the "Retrieve Last-Generated Assignments" function under the "Assignments" menu."""

    msg = MIMEMultipart()
    msg['From'] = MAIL_USERNAME
    msg['To'] = email
    msg['Subject'] = "Assignments Ready"
    msg.attach(MIMEText(message_txt, 'plain'))

    text = msg.as_string()
    mail.sendmail(MAIL_USERNAME, email, text)
    mail.quit()



def email_volunteers(volunteers: List[models.Volunteer]):
    mail = smtplib.SMTP('smtp.gmail.com', 587)
    mail.ehlo()
    mail.starttls()
    mail.login(MAIL_USERNAME, MAIL_PASSWORD)

    with open('email_plaintext.txt', 'r') as file:
        email_plaintext = MIMEText(file.read(), 'plain')

    with open('email_html.html', 'r') as file:
        email_html = MIMEText(file.read(), 'html')

    for volunteer in volunteers:
        msg = MIMEMultipart('alternative')
        
        msg['From'] = MAIL_USERNAME
        msg['To'] = volunteer.email
        msg['Subject'] = "Shift Assignment"

        this_email_plaintext = email_plaintext.replace('$NAME', volunteer.first_name + volunteer.last_name)
        this_email_html = email_html.replace('$NAME', volunteer.first_name + volunteer.last_name)

        msg.attach(this_email_plaintext)
        msg.attach(this_email_html)

        mail.sendmail(MAIL_USERNAME, volunteer.email, msg.as_string())

    mail.quit()
