# Common Cause - Volunteer Management System

The Common Cause Volunteer Management system is a solution designed to assist volunteer managers of the  Common Cause organization with tracking, storing, and updating volunteer and poll site information. In addition, the system provides tools to automatically generate assignments, find duplicate records, and generate custom reports regarding volunteer statistics.

The system runs the FastAPI web framework, and is coded in (almost) pure Python. SQLAlchemy manages all database operations.

This repository is intended to be cloned onto a virtual machine / server and be run by Gunicorn with Uvicorn ASGI worker processes.

Further instructions and details on the system can be found on the Developer Guide.