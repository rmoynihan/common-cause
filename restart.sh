#!/bin/bash

cd /lib/systemd/system
systemctl stop /lib/systemd/system cc-vm-system.service
systemctl daemon-reload
systemctl enable /lib/systemd/system cc-vm-system.service
systemctl start /lib/systemd/system cc-vm-system.service
